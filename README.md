# TypeScript Quickly

This repository contains code samples from the book "TypeScript Quickly" by Yakov Fain and Anton Moiseev. This book will by printed by Manning Publishers in the Summer of 2019. But you can start reading the completed chapter by getting the MEAP version of this book at https://www.manning.com/books/typescript-quickly

Chapter 10 implements a notifiication server that communicates with the nodes vie the WebSockect protocol. To see it in action, run npm install and then npm start. After that, open your browser at localhost:3000.
